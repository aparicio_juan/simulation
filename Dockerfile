FROM ubuntu:18.04

LABEL Juan Jose Aparicio <juanonlytechnology@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade && apt-get install -y \
  nano \
  python3.7 \
  python3.7-dev \
  python3-setuptools \
  python3-pip \
  nginx \
  supervisor \
  binutils \
  libproj-dev \
  sqlite3 \
  gdal-bin && \
  pip3 install -U pip setuptools && \
  rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install --upgrade pip

#https://uwsgi.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
RUN pip install uwsgi

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx-app.conf /etc/nginx/sites-available/default
COPY supervisor-app.conf /etc/supervisor/conf.d/

RUN mkdir -p /home/docker/code
VOLUME /home/docker/code

#for work in volume
COPY requirements.txt /home/docker/
RUN pip install -r /home/docker/requirements.txt
RUN mkdir /var/log/celery/ && \
  touch /var/log/celery/out_worker.log && \
  touch /var/log/celery/err_worker.log 

COPY celery* /etc/supervisor/conf.d/

#little color
ENV TERM="xterm-256color"
ENV GREP_OPTIONS="--color=auto"
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

EXPOSE 80
CMD ["supervisord", "-n"]
