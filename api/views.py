from django.shortcuts import get_object_or_404

from rest_framework import mixins, viewsets, status
from rest_framework.response import Response

from .serializers import (
    SimulationCreateSerializer,
    SimulationSerializer,
    SimulationResultSerializer,
)
from simulation.models import ArrivalRate, Cashier, Simulation
from simulation.utils_simulation import run_simulation


class SimulationViewSet(mixins.CreateModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    """ Simulation view set """

    serializer_class = SimulationCreateSerializer

    def list(self, request):
        queryset = Simulation.objects.all()
        serializer = SimulationSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        queryset = Simulation.objects.all()
        simulation = get_object_or_404(queryset, pk=pk)
        serializer = SimulationSerializer(simulation)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        """
        Create and run new simulation. receive data structure:
        {
            "n_prod_min": 2,
            "n_prod_max": 65,
            "proc_time": 12,
            "sim_time": 433,
            "cashiers": [
                {
                    "start_hour": 1,
                    "end_hour": 5,
                    "n": 3
                },
            ],
            "arrival_rate": [
                {
                    "hour": 2,
                    "arrivals": 323
                },
            ]
        }
        """
        
        # serializer and valid data
        serializer = SimulationCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        # create simulation
        simulation = Simulation.objects.create(
            n_prod_min=serializer.validated_data['n_prod_min'],
            n_prod_max=serializer.validated_data['n_prod_max'],
            proc_time=serializer.validated_data['proc_time'],
            sim_time=serializer.validated_data['sim_time'],
        )
       
        # create cashiers
        cashiers = serializer.data['cashiers']
        for cashier in cashiers:
            Cashier.objects.create(
                start_hour=cashier['start_hour'],
                end_hour=cashier['end_hour'],
                n=cashier['n'],
                simulation=simulation
            )

        # create arrival rate
        arrival_rate = serializer.data['arrival_rate']
        for arrival in arrival_rate:
            ArrivalRate.objects.create(
                hour=arrival['hour'],
                arrivals=arrival['arrivals'],
                simulation=simulation
            )

        # Run simulation without celery all sincrono   
        run_simulation(simulation.id)

        # Check is ok or failed and response
        simulate_status = Simulation.objects.get(id=simulation.id).status
        if simulate_status == 'Completed':
            serializer = SimulationResultSerializer(simulation.get_results.all()[0])
            # response results data it's ok
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"status":"Failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)








