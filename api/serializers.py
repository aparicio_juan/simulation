from rest_framework import serializers

from simulation.models import (
    Simulation, 
    Cashier, 
    ArrivalRate, 
    SimulationResult
)


class CashierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cashier
        fields = [
            'start_hour',
            'end_hour',
            'n'
        ]


class ArrivalRateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArrivalRate
        fields = [
            'hour',
            'arrivals'
        ]


class SimulationResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = SimulationResult
        fields = [
            'avg_wt',
            'avg_tis',
            'cant_people',
            'graph',
        ]
        read_only_fields = [
            'created',
        ]


class SimulationSerializer(serializers.ModelSerializer):

    cashiers = CashierSerializer(source="get_cashier", many=True)
    arrival_rate = ArrivalRateSerializer(source="get_arrival_rate", many=True)
    results = SimulationResultSerializer(source="get_results", many=True)
    
    class Meta:
        model = Simulation
        fields = [
            'n_prod_min',
            'n_prod_max',
            'proc_time',
            'sim_time',
            'status',
            'cashiers',
            'arrival_rate',
            'results'
        ]
        read_only_fields = [
            'created',
            'status',
        ]


class SimulationCreateSerializer(serializers.ModelSerializer):

    cashiers = CashierSerializer(source="get_cashier", many=True)
    arrival_rate = ArrivalRateSerializer(source="get_arrival_rate", many=True)
    
    class Meta:
        model = Simulation
        fields = [
            'n_prod_min',
            'n_prod_max',
            'proc_time',
            'sim_time',
            'cashiers',
            'arrival_rate',
        ]