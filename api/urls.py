from rest_framework.documentation import include_docs_urls
from rest_framework.routers import DefaultRouter

from django.urls import path, include

from .views import SimulationViewSet

router = DefaultRouter()
router.register(r'simulation', SimulationViewSet, basename='simulation')

urlpatterns = [
    path('v1/api-docs/', include_docs_urls(title='Test')),
    path('v1/', include(router.urls)),
]
