app.component('box-arrivals-data', {
    template: `
    <div class="col-md-3 mt-3 mb-3">
        <label :for="idHour">Arrival hour:</label> 
        <div>
            <input class="form-control" required type="number" :value="hour" :name="nameHour" :id="idHour">
        </div>
        <label :for="idArrivals">Average number of people:</label> 
        <div>
            <input class="form-control" required type="number" :value="arrivals" :name="nameArrivals" :id="idArrivals">
        </div>
    </div>`,
    props: {
           
        hour: {
            type: Number,
            required: false,
        },
        arrivals: {
            type: Number,
            required: false
        },
        id: {
            type: Number,
            required: true,
        }
    },
    computed: {
        // it's necessary for process formset en backend
        nameHour(){
            return "form-"+this.id+"-hour"
        },
        idHour(){
            return "id_form-"+this.id+"-hour"
        },
        nameArrivals(){
            return "form-"+this.id+"-arrivals"
        },
        idArrivals(){
            return "id_form-"+this.id+"-arrivals"
        },
    },
    delimiters: ["[[", "]]"]
}
)