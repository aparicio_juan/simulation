const app = Vue.createApp({
    data() {
        return {
            cashier: [{ start_hour: null, end_hour: null, n: 1, }],
            arrivalData: [{ hour: null, arrivals: null, }],
            cant_cashier: 1,
            cant_arrivals: 1
        }
    },
     methods: {
         // buttons controlers
         addCashier(){
             const data = {
                start_hour: null, 
                end_hour: null, 
                n: 1,
             };
             this.cashier.push(data);
             this.cant_cashier++;
         },
         removeCashier(){
             this.cashier.pop();
             this.cant_cashier--;
         },
         addArrivalData(){
             const data = {
                hour: null, 
                arrivals: null
             };
             this.arrivalData.push(data);
             this.cant_arrivals++;
         },
         removeArrivalData(){
             this.arrivalData.pop();
             this.cant_arrivals--;
         }
    },
    delimiters: ['[[', ']]'],
})


