from django.core.files.base import ContentFile

import numpy as np
import matplotlib.pyplot as plt
import logging

from simulation.simulation import main
from simulation.models import ArrivalRate, Cashier, Simulation, SimulationResult


logger = logging.getLogger(__name__)


def get_simulation(id):
    return Simulation.objects.get(id=id)


def get_cashiers(simulation):
    """ get cashier list """

    CASHIER_DATA = []
    cashiers = Cashier.objects.filter(
            simulation=simulation
            ).values_list(
                'start_hour',
                'end_hour',
                'n'
    )
    for x in range(len(cashiers)):
        for _ in range(cashiers[x][2]):
            CASHIER_DATA.append([cashiers[x][0], cashiers[x][1]])
    return CASHIER_DATA


def get_arrival_data(simulation):
    """ get arrival data list """

    arrivals_data = list(ArrivalRate.objects.filter(
            simulation=simulation
            ).values_list(
                'hour',
                'arrivals'
            )
    )
    return arrivals_data


def process_results_data(stats):
    stats = [s for s in stats if s.leave is not None]
    arrival = np.array([s.arrival for s in stats])
    queue = np.array([s.queue for s in stats])
    process = np.array([s.process for s in stats])
    leave = np.array([s.leave for s in stats])   
    wait_time = np.average(process - queue)
    time_in_system = np.average(leave - arrival)
    
    data = {
        "stats": stats,
        "arrival": arrival,
        "queue": queue,
        "process": process,
        "leave": leave,   
        "wait_time": wait_time,
        "time_in_system": time_in_system
    }
    return data


def save_result(data, simulation):
    """ Save result on database """

    results = SimulationResult(
        avg_wt=round(data['wait_time'], 2),
        avg_tis=round(data['time_in_system'], 2),
        cant_people=data['arrival'].shape[0],
        simulation=simulation
    )
    results.save()
    graph_generator([data['arrival'],data['queue'],data['process'],data['leave']], results)


def run_simulation(id_simulation):
    simulation = get_simulation(id_simulation)
    try:
        stats = []
        CASHIER_DATA = get_cashiers(simulation)
        ARRIVAL_DATA =get_arrival_data(simulation)
        RANDOM_SEED = 42
        SIM_TIME = simulation.sim_time
        PROC_TIME = simulation.proc_time
        MIN_PRODS = simulation.n_prod_min
        MAX_PRODS = simulation.n_prod_max
        simulation.status = "Running"
        simulation.save()
        main(RANDOM_SEED, SIM_TIME, CASHIER_DATA, ARRIVAL_DATA, PROC_TIME, MIN_PRODS, MAX_PRODS, stats)
        simulation.status = "Completed"
        simulation.save()
        processed_data = process_results_data(stats)
        save_result(processed_data, simulation)
    except Exception as e:
        logger.error('[-] Error: {}'.format(e))
        simulation.status = "Failed"
        simulation.save()



def graph_generator(data, results):
    ''' 
    graph generator, recive data = [arrival, queue, process,leave]
    '''

    try:
        colors = ['blue', 'red', 'orange', 'grey']
        names = [ "Arrival", "Queue", "Process", "Leave"]

        # graph generator
        plt.figure(dpi=150)
        for serie, color, name in zip(data, colors, names):
            plt.plot(serie, label=name, color=color)
        plt.legend()
        plt.xlabel("People ", size = 16,)
        plt.ylabel("Time (minutes)", size = 16)
        plt.savefig("graph.jpg")

        # retrieve grahp and save in object results
        with open('graph.jpg', 'rb') as f:
            data = f.read()
        results.graph.save(str(results.id)+'.jpg',  ContentFile(data))
    except Exception as e:
        logger.error('[-] Error: {}, graph not generate'.format(e))


