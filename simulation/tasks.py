from simulation.utils_simulation import run_simulation

from celery import shared_task


@shared_task
def run_simulation_asinc(id_simulation):
    run_simulation(id_simulation)
