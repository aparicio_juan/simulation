from django.views.generic import ListView, FormView, DetailView
from django.urls import reverse_lazy

import logging

from simulation.models import ArrivalRate, Cashier, Simulation
from simulation.forms import SimulationForm, CashierFormSet, ArrivalRateFormSet
from simulation.tasks import run_simulation_asinc


logger = logging.getLogger(__name__)


class SimulationsListView(ListView):
    template_name = 'simulation/list-simulation.html'
    model = Simulation
    context_object_name = 'simulations'


class SimulationNewView(FormView):
    template_name = 'simulation/new-simulation.html'
    form_class = SimulationForm
    extra_context={'form2':CashierFormSet} # It's necessary for an extra fields from formset
    success_url = reverse_lazy('simulation_list_view')

    def save_formset(self, formset, simulation):
        if formset.is_valid():
            for form in formset:
                if form.is_valid():
                    form = form.save(commit=False)
                    form.simulation = simulation
                    form.save()
        else:
            logger.error('[-] Error save formset: {}'.format(formset.errors))
            

    def post(self, request, *args, **kwargs):
        
        # process data simulation
        form = SimulationForm(request.POST)
        if form.is_valid():
            simulation = form.save(commit=False)
            simulation.save()

        # copy to new POST bacause need modify it
        formset_dictionary_copy = request.POST.copy()
        
        # process data cashier and arrivals in formset
        # rewrite form-TOTAL_FORMS with total the same type, it's necessary to process formset
        formset_dictionary_copy['form-TOTAL_FORMS'] = request.POST.get('cant_cashier', '1')
        CashierSet = CashierFormSet(formset_dictionary_copy)
        self.save_formset(CashierSet, simulation)

        formset_dictionary_copy['form-TOTAL_FORMS'] = request.POST.get('cant_arrivals', '1')
        ArrivalsSet = ArrivalRateFormSet(formset_dictionary_copy)
        self.save_formset(ArrivalsSet, simulation)
       
        # run simulation in celery
        run_simulation_asinc.delay(simulation.id)

        return super().post(request, *args, **kwargs)


class SimulationDetailView(DetailView):
    template_name = 'simulation/detail-simulation.html'
    model = Simulation
    context_object_name = 'simulation'

    def get_context_data(self, **kwargs):
        """Insert extra objects into the context dict."""
        context = {}
        context['cashiers'] = Cashier.objects.filter(simulation=self.object)
        context['arraivals_data'] = ArrivalRate.objects.filter(simulation=self.object)
        context.update(kwargs)
        return super().get_context_data(**context)