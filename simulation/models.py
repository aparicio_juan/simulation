from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse

class Simulation(models.Model):
    #user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    n_prod_min = models.IntegerField()
    n_prod_max = models.IntegerField()
    proc_time = models.FloatField()
    replica = models.IntegerField(default=1)
    sim_time = models.PositiveIntegerField()
    status = models.CharField(default="Create", max_length=12)
    created = models.DateTimeField(auto_now_add=True)

    def  __str__(self) -> str:
        return 'Simulation {}'.format(self.id)

    def get_absolute_url(self):
        return reverse("simulation_detail_view", kwargs={"pk": self.pk})

    class Meta:
        ordering = ['-created']


class Cashier(models.Model):
    start_hour = models.IntegerField()
    end_hour = models.IntegerField()
    n = models.IntegerField()
    simulation = models.ForeignKey(
        Simulation, 
        on_delete=models.CASCADE,
        related_name='get_cashier'
    )


class ArrivalRate(models.Model):
    hour = models.IntegerField()
    arrivals = models.IntegerField()
    simulation = models.ForeignKey(
        Simulation, 
        on_delete=models.CASCADE,
        related_name='get_arrival_rate'
    )


class SimulationResult(models.Model):
    avg_wt = models.FloatField()
    avg_tis = models.FloatField()
    cant_people = models.IntegerField()
    simulation = models.ForeignKey(
        Simulation, 
        on_delete=models.CASCADE,
        related_name='get_results'
    )
    created = models.DateTimeField(auto_now_add=True)
    graph = models.ImageField(
        blank=True, 
        null=True,
        upload_to ='uploads/'
        )

