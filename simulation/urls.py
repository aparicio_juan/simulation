from django.urls import path

from .views import SimulationsListView, SimulationNewView, SimulationDetailView


urlpatterns = [
    path('', SimulationsListView.as_view(), name='simulation_list_view'),
    path('new-simulation', SimulationNewView.as_view(), name='simulation_new_view'),
    path('detail-simulation/<pk>/', SimulationDetailView.as_view(), name='simulation_detail_view'),
]