from django import forms
from django.forms import formset_factory
from simulation.models import Simulation, Cashier, ArrivalRate


class SimulationForm(forms.ModelForm):

    class Meta:
        model = Simulation
        fields = [
            'n_prod_min',
            'n_prod_max',
            'proc_time',
            'sim_time',
        ]
        labels = {
            'n_prod_min':'Minimum products',
            'n_prod_max':'Maximum products',
            'proc_time':'Time per product',
            'sim_time': 'Amount of time the simulation runs (Minutes)'
        }
        widgets = { 
            'n_prod_min': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': "Minimum quantity of products"
                    }
            ),
            'n_prod_max': forms.NumberInput(
                attrs={
                    'class': 'form-control', 
                    'placeholder': "Maximum quantity of products"
                    }
            ),
            'proc_time': forms.NumberInput(
                attrs={
                    'class': 'form-control', 
                    'placeholder': "Process time per product"
                    }
            ),
            'sim_time': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': "Amount of time the simulation runs (Minutes)",
            }
        )
    }


class CashierForm(forms.ModelForm):

    class Meta:
        model = Cashier
        fields = [
            'start_hour',
            'end_hour',
            'n',
        ]


class ArrivalRateForm(forms.ModelForm):

    class Meta:
        model = ArrivalRate
        fields = [
            'hour',
            'arrivals',
        ]


CashierFormSet = formset_factory(CashierForm)

ArrivalRateFormSet = formset_factory(ArrivalRateForm)