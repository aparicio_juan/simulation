from django.contrib import admin

from simulation.models import Simulation, Cashier, ArrivalRate, SimulationResult

admin.site.register(Simulation)
admin.site.register(Cashier)
admin.site.register(ArrivalRate)
admin.site.register(SimulationResult)