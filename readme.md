Resolución Requerimientos propuestos:

1.- Para cumplimentar el requerimiento se utilizaron class views de django, en simulation/views.py
las cuales utilizar los formularios de simulation/forms.py para validar los datos y guardarlos en los
modelos de simulation/models.py.
Para el formulario que permite crear y generar la simulación en Front-end aparte de renderizar
formulario básico de django para la primera parte, se utilizó componentes de Vuejs para generar una
cantidad ilimitada de cashiers y arrivals rate, los cuales son procesados en el backend como formset.
Una vez procesada la información se procede a generar la simulación en forma asincrona con
celery. Para preparar los datos para la generación de las simulaciones se creo componentes de esta
en simulation/utils_simulation.py y se utilizo los modulos proporcionados en
simulation/simulation.py, manteniendo de esta manera separado todo el proceso y preparación de la
simulacion de las vistas para su reutilización y no modificando el codigo heredado para la misma.

2.- Se creo una class view de django para renderizar el listado de simulaciones creadas.

3.- se creo una class view para mostrar el detalle de la simulación, junto con su resultado, estado,
gráfica y datos utilizados para generarla, se accede a esto desde la lista generada en el punto 2.



Resolución requerimientos deseables:

1.- Se creo un app nueva llamada api en la cual se utiliza DRF para crear las apis de listado, detalle
y creación/activar de simulación de la misma. Para las mismas se utilizo viewsets y routers.
Así mismo se ha instalado un modulo de documentación de las API para ver y probar su utilización,
accediendo a este mediante la url /api/v1/api-docs/ donde dentro están las urls de todas las apis.

2.- Se instaló y configuro celery usando redis. La simulación corre sobre esta solo desde la web si se
utiliza la api para generarla esta no corre por celery, a la necesidad de que por medio de la api no
sea asincrona los estados no se configuraron desde celery sino desde la generación de la simulación
y asi poder registrarlos tanto si corre por celery como si no.

3.- Se creo un gráfico simple de los datos generados por la simulación mediante el paquete
matplotlib, este gráfico es guardado en el modelo de resultados para que solo sea necesario
generarlo cuando se corre la simulación y asi compartirlo por medio de la API como archivo media
y también es mostrado en la vista de detalle de la simulación. (El gráfico es simple solo se muestran
las 4 lineas de tiempo que se generan, no soy bueno con esto y solo me limite a mostrar esos datos
solamente)


Stack Frontend: HTML, CSS, Bootstrap5, Vuejs.

Stack Backend: Python, Django, DRF, Celery, Redis.

Stack deploy: Docker, Docker-compose, Nginx, Uwsgi, Supervisor.

NOTA: Se entrega listo para probar desplegando con docker-compose.