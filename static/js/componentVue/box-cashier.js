app.component('box-cashier', {
    template: `
    <div class="col-md-3 mt-3 mb-3">
        <label :for="idStart">Start hour:</label> 
        <div>
        <input class="form-control" required type="number" :value="start_hour" min="1" max="24" :name="nameStart" :id="idStart">
        </div>
        <label :for="idEnd">End hour:</label> 
        <div>
        <input class="form-control" required  type="number" :value="end_hour"  min="1" max="24" :name="nameEnd" :id="idEnd"></div>
        <label :for="idN">Number of equal cashier:</label> 
        <div>
        <input class="form-control" required type="number" value="1" :name="nameN" :id="idN">
        </div>
    </div>`,
    props: {
        start_hour: {
            type: Number,
            required: false,
        },
        end_hour: {
            type: Number,
            required: false
        },
        n: {
            type: Number,
            required: false,
            default: 1
        },
        id: {
            type: Number,
            required: true,
        }
    },
    computed: {
        // it's necessary for process formset en backend
        nameStart(){
            return "form-"+this.id+"-start_hour"
        },
        idStart(){
            return "id_form-"+this.id+"-start_hour"
        },
        nameEnd(){
            return "form-"+this.id+"-end_hour"
        },
        idEnd(){
            return "id_form-"+this.id+"-end_hour"
        },
        nameN(){
            return "form-"+this.id+"-n"
        },
        idN(){
            return "id_form-"+this.id+"-n"
        },
    },
    delimiters: ["[[", "]]"]
}
)